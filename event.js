const mongoose = require('mongoose')

const { Schema, SchemaTypes } = mongoose

const eventSchema = new Schema({
  id: SchemaTypes.ObjectId,
  uuid: { type: String, index: true },
  name: String,
  openQueue: Date,
  closeQueue: Date,
  maxOutflowAmount: Number,
  sessionTime: Number,
  arrivalTime: Number,
  isActive: Boolean,
  isScheduled: Boolean,
  currentQueueNumber: Number,
  redirectURL: String,
})

const EventModel = mongoose.model('Event', eventSchema)

module.exports = { eventSchema, EventModel }
