const { Schema, SchemaTypes } = require('mongoose')

const { mongoose } = require('../config/mongoose')


const audienceSchema = new Schema({
  id: SchemaTypes.ObjectId,
  uuid: { type: String, index: true },
  queueNumber: { type: Number, default: 1, unique: true },
  telno: String,
  email: String,
  status: { type: String, index: true },
  enteredQueueAt: Date,
  sessionTimedoutAt: Date,
  arrivalTimedoutAt: Date,
  eligibledAt: { type: Date, index: true },
  servedAt: { type: Date, index: true },
  canceledAt: Date,
  completedAt: Date,
})

const AudienceModel = mongoose.model('Audience', audienceSchema)

module.exports = { audienceSchema, AudienceModel }
