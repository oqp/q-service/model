const mongoose = require('mongoose')

const { Schema, SchemaTypes } = mongoose

const { eventSchema } = require('./event')

const waitingRoomSchema = new Schema({
  id: SchemaTypes.ObjectId,
  name: String,
  website: String,
  subdomain: String,
  uuid: String,
})

const WaitingRoomModel = mongoose.model('WaitingRoom', waitingRoomSchema)

module.exports = { waitingRoomSchema, WaitingRoomModel }
